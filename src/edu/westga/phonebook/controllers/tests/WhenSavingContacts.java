package edu.westga.phonebook.controllers.tests;

import edu.westga.phonebook.controllers.ContactsController;
import edu.westga.phonebook.model.Contact;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Tests for the saveTo() method of ContactsController.
 * 
 * @author lewisb
 *
 */
public class WhenSavingContacts {

	private ContactsController controller;
	private Contact alice;
	private Contact bob;
	private Contact cindy;
	
	@Rule
	public TemporaryFolder folder = new TemporaryFolder();
	
	private File saveFile;
	
	/**
	 * Called before all edu.westga.phonebook.controllers.tests in this suite.
	 * 
	 * @throws Exception n/a
	 */
	@Before
	public void setUp() throws Exception {
		this.controller = new ContactsController();
		this.alice = new Contact("Alice");
		this.alice.setEmail("alice@alice.com");
		this.alice.setPhone("123-456-7890");
		
		this.bob = new Contact("Bob");
		this.bob.setEmail("bob@gmail.com");
		this.bob.setPhone("404-867-5309");
		
		this.cindy = new Contact("Cindy");
		this.cindy.setEmail("cindy@mail.yahoo.com");
		this.cindy.setPhone("770-999-4455");
		
		this.saveFile = this.folder.newFile();
	}

	/**
	 * Case where there are no contacts to save (file should be empty)
	 * 
	 * @throws IOException shouldn't happen!
	 */
	@Test
	public void shouldNotSaveAnyContacts() throws IOException {
		this.controller.saveTo(this.saveFile);
		Scanner scanner = new Scanner(this.saveFile);
		assertFalse(scanner.hasNextLine());
		scanner.close();
	}
	
	/**
	 * Case where one contact is saved
	 * 
	 * @throws IOException shouldn't happen!
	 */
	@Test
	public void shouldSaveOneContact() throws IOException {
		this.controller.add(this.alice);
		this.controller.saveTo(this.saveFile);
		Scanner scanner = new Scanner(this.saveFile);
		String actual = scanner.nextLine();
		assertEquals("Alice,alice@alice.com,123-456-7890", actual);
		assertFalse(scanner.hasNextLine());
		scanner.close();
	}
	
	/**
	 * Case where multiple contacts are saved
	 * 
	 * @throws IOException shouldn't happen!
	 */
	@Test
	public void shouldSaveMayContacts() throws IOException {
		this.controller.add(this.alice);
		this.controller.add(this.bob);
		this.controller.add(this.cindy);
		this.controller.saveTo(this.saveFile);
		Scanner scanner = new Scanner(this.saveFile);
		String actual = scanner.nextLine();
		assertEquals("Alice,alice@alice.com,123-456-7890", actual);
		actual = scanner.nextLine();
		assertEquals("Bob,bob@gmail.com,404-867-5309", actual);
		actual = scanner.nextLine();
		assertEquals("Cindy,cindy@mail.yahoo.com,770-999-4455", actual);
		assertFalse(scanner.hasNextLine());
		scanner.close();
	}

}
