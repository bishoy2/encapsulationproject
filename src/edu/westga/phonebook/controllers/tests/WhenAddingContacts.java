package edu.westga.phonebook.controllers.tests;

import edu.westga.phonebook.controllers.ContactsController;
import edu.westga.phonebook.model.Contact;
import edu.westga.phonebook.model.IContactAccessor;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.lang.ClassCastException;

/**
 * Tests for the add() method of ContactsController
 * 
 * @author lewisb
 *
 */
public class WhenAddingContacts {

	private ContactsController controller;
	
	/**
	 * Called before all edu.westga.phonebook.controllers.tests in this suite.
	 * 
	 * @throws Exception not used
	 */
	@Before
	public void setUp() throws Exception {
		this.controller = new ContactsController();
	}

	
	@Test
	public void shouldAddOneContact() throws ClassCastException {
		this.controller.add("Joe");
		Contact[] contacts = (Contact[]) this.controller.getContacts();
		assertEquals(1, contacts.length);
		assertEquals("Joe",contacts[0].getName());
	}

	/**
	 * Case where two contacts have been added, and are added in their sorted order.
	 */
	@Test
	public void shouldAddTwoContactsInOrder() {
		this.controller.add("Anna");
		this.controller.add("Bob");
		IContactAccessor[] contacts = this.controller.getContacts();
		assertEquals(2, contacts.length);
		assertEquals("Anna", ((Contact) contacts[0]).getName());
		assertEquals("Bob", ((Contact) contacts[1]).getName());
	}
	
	/**
	 * Case where two contacts have been added, and are added in their unsorted order.
	 */
	@Test
	public void shouldAddTwoContactsOutOfOrder() {
		this.controller.add("Bob");
		this.controller.add("Anna");
		IContactAccessor[] contacts = this.controller.getContacts();
		assertEquals(2, contacts.length);
		assertEquals("Anna", contacts[0].getName());
		assertEquals("Bob", contacts[1].getName());
	}
	
	/**
	 * Case where a bunch of contacts have been added and were added in the sorted order
	 */
	@Test
	public void shouldAddManyContactsInOrder() {
		this.controller.add("Anna");
		this.controller.add("Bob");
		this.controller.add("Cindy");
		this.controller.add("Dave");
		this.controller.add("Elena");
		IContactAccessor[] contacts = this.controller.getContacts();
		assertEquals(5, contacts.length);
		assertEquals("Anna", contacts[0].getName());
		assertEquals("Bob", contacts[1].getName());
		assertEquals("Cindy", contacts[2].getName());
		assertEquals("Dave", contacts[3].getName());
		assertEquals("Elena", contacts[4].getName());
	}
	
	/**
	 * Case where a bunch of contacts have been added and were added in no particular order
	 */
	@Test
	public void shouldAddManyContactsOutOfOrder() {
		this.controller.add("Elena");
		this.controller.add("Anna");
		this.controller.add("Dave");
		this.controller.add("Cindy");
		this.controller.add("Bob");
		IContactAccessor[] contacts = this.controller.getContacts();
		assertEquals(5, contacts.length);
		assertEquals("Anna", contacts[0].getName());
		assertEquals("Bob", contacts[1].getName());
		assertEquals("Cindy", contacts[2].getName());
		assertEquals("Dave", contacts[3].getName());
		assertEquals("Elena", contacts[4].getName());
	}
}
