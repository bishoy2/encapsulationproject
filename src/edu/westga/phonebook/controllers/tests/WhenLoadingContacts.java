package edu.westga.phonebook.controllers.tests;

import edu.westga.phonebook.controllers.ContactsController;
import edu.westga.phonebook.model.Contact;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import static org.junit.Assert.assertEquals;

/**
 * Tests of the loadFrom() method of ContactsController
 * 
 * @author lewisb
 *
 */
public class WhenLoadingContacts {

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();
	
	private ContactsController controller;
	private File infile;
	
	/**
	 * Called before all test methods in this suite.
	 * 
	 * @throws Exception shouldn't happen!
	 */
	@Before
	public void setUp() throws Exception {
		this.controller = new ContactsController();
		this.infile = this.folder.newFile();
	}

	/**
	 * Case where the input file contains no contacts.
	 * 
	 * @throws FileNotFoundException shouldn't happen
	 */
	@Test
	public void shouldLoadEmptyFile() throws FileNotFoundException {
		this.controller.loadFrom(this.infile);
		Contact[] contacts = (Contact[]) this.controller.getContacts();
		assertEquals(0, contacts.length);
	}
	
	/**
	 * Case where the input file contains a single contact.
	 * 
	 * @throws FileNotFoundException shouldn't happen
	 */
	@Test
	public void shouldLoadFileWithSingleContact() throws FileNotFoundException {
		PrintWriter writer = new PrintWriter(this.infile);
		writer.println("Alice,alice@westga.edu,111-222-3333");
		writer.close();
		this.controller.loadFrom(this.infile);
		Contact[] contacts = (Contact[]) this.controller.getContacts();
		assertEquals(1, contacts.length);
		assertEquals("Alice", contacts[0].getName());
		assertEquals("alice@westga.edu", contacts[0].getEmail());
		assertEquals("111-222-3333", contacts[0].getPhone());
	}

	/**
	 * Case where the input file contains several contacts
	 * 
	 * @throws FileNotFoundException shouldn't happen
	 */
	@Test
	public void shouldLoadFileWithManyContacts() throws FileNotFoundException {
		PrintWriter writer = new PrintWriter(this.infile);
		writer.println("Alice,alice@westga.edu,111-222-3333");
		writer.println("Bob,bob@gmail.com,404-867-5309");
		writer.println("Cindy,cindy@mail.yahoo.com,123-456-7890");
		writer.close();
		this.controller.loadFrom(this.infile);
		Contact[] contacts = (Contact[]) this.controller.getContacts();
		assertEquals(3, contacts.length);
		assertEquals("Alice", contacts[0].getName());
		assertEquals("alice@westga.edu", contacts[0].getEmail());
		assertEquals("111-222-3333", contacts[0].getPhone());
		assertEquals("Bob", contacts[1].getName());
		assertEquals("bob@gmail.com", contacts[1].getEmail());
		assertEquals("404-867-5309", contacts[1].getPhone());
		assertEquals("Cindy", contacts[2].getName());
		assertEquals("cindy@mail.yahoo.com", contacts[2].getEmail());
		assertEquals("123-456-7890", contacts[2].getPhone());
	}
}
