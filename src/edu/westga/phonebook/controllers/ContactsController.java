package edu.westga.phonebook.controllers;

import edu.westga.phonebook.model.Contact;
import edu.westga.phonebook.model.IContactAccessor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * A Controller for managing Contact objects.
 * 
 * @author lewisb
 *
 */
public class ContactsController {

	private ArrayList<Contact> phonebook;
	
	/**
	 * Creates a new ContactsController.
	 */
	public ContactsController() {
		this.phonebook = new ArrayList<Contact>();
	}
	
	/**
	 * Copy Constructor for ContactsController
	 * @param contactsController
	 */
	public ContactsController(ContactsController contactsController) {
		this.phonebook = new ArrayList<Contact>(contactsController.phonebook);
	}
	
	/**
	 * Gets all Contact objects as an array
	 * 
	 * @return all Contact objects
	 */
	public IContactAccessor[] getContacts() {
		return this.phonebook.toArray(new Contact[this.phonebook.size()]);
	}
	
	/**
	 * Gets a specific Contact
	 * 
	 * @param index the index of the Contact
	 * @return the desired Contact
	 */
	public IContactAccessor getContact(int index) {
		return this.phonebook.get(index);
	}
	
	/**
	 * Adds a Contact if that Contact is not already in the phonebook.
	 * 
	 * @param name the name of the contact to add.
	 */
	public void add(String name) {
		Contact contact = new Contact(name);
		this.add(contact);
	}


	/**
	 * Adds a Contact if that Contact is not already in the phonebook.
	 * 
	 * @param contact the Contact to add
	 */
	public void add(Contact contact) {
		if (this.searchByName(contact.getName()) != null) {
			return;
		}
		
		this.phonebook.add(contact);
		Collections.sort(this.phonebook);
	}
	
	/**
	 * Deletes a contact
	 * 
	 * @param index the index of the contact to delete
	 */
	public void remove(int index) {
		this.phonebook.remove(index);
	}
	
	/**
	 * Changes the email and phone number of the Contact at the given index
	 * 
	 * @param index the index of the Contact
	 * @param email the new email address for the Contact
	 * @param phone the new phone number for the Contact
	 */
	public void edit(int index, String email, String phone) {
		Contact contact = this.phonebook.get(index);
		contact.setEmail(email);
		contact.setPhone(phone);
	}
	
	/**
	 * Searches for a Contact by name.
	 * 
	 * @param name the name of the Contact we're searching for
	 * @return the desired Contact, or null if none found
	 */
	public IContactAccessor searchByName(String name) {
		for (Contact contact: this.phonebook) {
			if (contact.getName().equals(name)) {
				return contact;
			}
		}
		return null;
	}
	
	/**
	 * Saves the list of contacts to a file.  Each line of the file will have the format:
	 * name,email,phone
	 * 
	 * @param filename the filename to save to
	 * @throws IOException if the file can not be opened for saving
	 */
	public void saveTo(String filename) throws IOException {
		this.saveTo(new File(filename));
	}
	
	/**
	 * Saves the list of contacts to a file.  Each line of the file will have the format:
	 * name,email,phone
	 * 
	 * @param outfile the file to save to
	 * @throws IOException if the file can not be opened for saving
	 */
	public void saveTo(File outfile) throws FileNotFoundException {
		PrintWriter writer = new PrintWriter(outfile);
		for (Contact contact: this.phonebook) {
			String output = String.format("%s,%s,%s", contact.getName(), contact.getEmail(), contact.getPhone());
			writer.println(output);
		}
		writer.close();
	}
	
	/**
	 * Loads phonebook data from a text file.  The data is in comma-separated format, one contact per line, for example:
	 * <pre>
	 * Alice,alice@alice.com,123-456-789
	 * Bob,bob@gmail.com,404-867-5309
	 * Cindy,cindy@mail.yahoo.com,111-222-3333
	 * </pre>
	 * There can be any number of contacts in the file (including zero)
	 * 
	 * @param infile the file to load from
	 * @throws FileNotFoundException if the file isn't found
	 */
	public void loadFrom(File infile) throws FileNotFoundException {
		Scanner scanner = new Scanner(infile);
		this.phonebook.clear();
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			String[] parts = line.split(",");
			String name = parts[0];
			String email = parts[1];
			String phone = parts[2];
			Contact contact = new Contact(name);
			contact.setEmail(email);
			contact.setPhone(phone);
			this.phonebook.add(contact);
		}
		scanner.close();
	}
	
	/**
	 * Loads phonebook data from a text file.  The data is in comma-separated format, one contact per line, for example:
	 * <pre>
	 * Alice,alice@alice.com,123-456-789
	 * Bob,bob@gmail.com,404-867-5309
	 * Cindy,cindy@mail.yahoo.com,111-222-3333
	 * </pre>
	 * There can be any number of contacts in the file (including zero)
	 * 
	 * @param filename the name of the file to load
	 * 
	 * @throws FileNotFoundException if the file can't be found
	 */
	public void loadFrom(String filename) throws FileNotFoundException {
		this.loadFrom(new File(filename));
	}
}
