package edu.westga.phonebook.views;

import edu.westga.phonebook.controllers.ContactsController;
import edu.westga.phonebook.model.IContactAccessor;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * A PhonebookView lets a user do the following:
 * <ul>
 * <li>add a Contact</li>
 * <li>delete a Contact</li>
 * <li>edit an existing Contact</li>
 * <li>search for a contact by name</li>
 * <li>display all contacts</li>
 * <li>display a specific contact</li>
 * </ul>
 * 
 * @author lewisb
 *
 */
public class PhonebookView {

	private ContactsController controller;
	
	/**
	 * Creates a new PhonebookView
	 */
	public PhonebookView() {
		this.controller = new ContactsController(this.controller);
	}
	
	/**
	 * Processes a command string.  Valid commands are:
	 * <ul>
	 * <li>ADD contact_name : adds a new Contact where contact_name is the contact's name
	 * <li>DELETE index : deletes the Contact at the given index
	 * <li>EDIT index new_email new_phone  : lets the user edit the info for the Contact at the given index
	 * <li>SAVE : saves the current phonebook to contacts.csv
	 * <li>LOAD : loads the contacts from contacts.csv
	 * <li>HELP : prints the help message
	 * <li>QUIT : quits the program
	 * </ul>
	 * @param command the command to process.
	 * @return true if the program should continue; false if the program should end
	 */
	public boolean processCommand(String command) {
		String[] parts = command.split(" ");
		String method = parts[0];
		if (method.equalsIgnoreCase("ADD")) {
			this.processAddCommand(parts[1]);
		} else if (method.equalsIgnoreCase("DELETE")) {
			this.processDeleteCommand(Integer.parseInt(parts[1]));
		} else if (method.equalsIgnoreCase("EDIT")) {
			this.processEditCommand(Integer.parseInt(parts[1]), parts[2], parts[3]);
		} else if (method.equalsIgnoreCase("QUIT")) {
			return false;
		} else if (method.equalsIgnoreCase("HELP")) {
			this.printHelp();
		} else if (method.equalsIgnoreCase("SAVE")) {
			this.processSaveCommand();
		} else if (method.equalsIgnoreCase("LOAD")) {
			this.processLoadCommand();
		} else {
			this.handleBadCommand();
		}
		
		this.printPhonebook();
		return true;
	}

	/**
	 * Processes the LOAD command
	 */
	private void processLoadCommand() {
		try {
			this.controller.loadFrom("contacts.csv");
		} catch (FileNotFoundException e) {
			System.out.println("Could not find file: contacts.csv");
		}		
	}

	/**
	 * Process the SAVE command.
	 */
	private void processSaveCommand() {
		try {
			this.controller.saveTo("contacts.csv");
		} catch (IOException e) {
			System.out.println("Could not open file for saving");
		}		
	}

	/**
	 * Called when a bad command is issued.
	 */
	private void handleBadCommand() {
		System.out.println("Bad command, try again.");
	}

	/**
	 * Prints the current state of the phonebook
	 */
	private void printPhonebook() {
		IContactAccessor[] contacts = this.controller.getContacts();
		for (int i = 0; i < contacts.length; i++) {
			String name = contacts[i].getName();
			String email = contacts[i].getEmail();
			String phone = contacts[i].getPhone();
			String line = String.format("%2d: %15s %15s %10s", i, name, email, phone);
			System.out.println(line);
		}
	}
	
	/**
	 * Processes the EDIT command
	 * 
	 * @param index index of the contact to edit
	 * @param email the new email address for the contact
	 * @param phone the new phone number for the contact
	 */
	private void processEditCommand(int index, String email, String phone) {
		this.controller.edit(index, email, phone);
	}

	/**
	 * Processes the DELETE command
	 * 
	 * @param index the index of the contact to delete
	 */
	private void processDeleteCommand(int index) {
		this.controller.remove(index);
		
	}

	/**
	 * Prints the help message for the program
	 */
	private void printHelp() {
		System.out.println("Valid commands are:");
		System.out.println("HELP                   : prints this help");
		System.out.println("ADD name               : adds a new Contact with the given name");
		System.out.println("DELETE index           : deletes the Contact at that index");
		System.out.println("EDIT index email phone : changes the email and phone of the contact at that index");
		System.out.println("QUIT                   : quits the program");
	}
	
	/**
	 * Processes the ADD command
	 * 
	 * @param name name of the contact to add
	 */
	private void processAddCommand(String name) {
		this.controller.add(name);
	}
}
