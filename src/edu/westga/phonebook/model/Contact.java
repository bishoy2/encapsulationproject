package edu.westga.phonebook.model;

/**
 * A Contact is a single entry in a Phonebook. It has fields for name, email address, and
 * phone number.  The name field is required, all others are optional.
 * 
 * @author lewisb
 *
 */
public class Contact extends Object implements Comparable<Contact>, IContactAccessor {

	private final String name;
	private String email;
	private String phone;
	
	/**
	 * Creates a new Contact with the given name
	 * 
	 * @param name the Contact's name
	 */
	public Contact(String name) {
		this.name = name;
		this.email = "";
		this.phone = "";
	}

	/**
	 * Copy constructor
	 */
	public Contact(IContactAccessor contactAccessor) {
		this.name = contactAccessor.getName();
		this.email = contactAccessor.getEmail();
		this.phone = contactAccessor.getPhone();
	}
	
	public String getName() {
		return this.name;
	}
	

	public String getEmail() {
		return this.email;
	}
	
	
	/**
	 * Sets this Contact's email address.
	 * 
	 * @param email this Contact's new email address
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * Gets this Contact's phone number
	 * 
	 * @return this Contact's phone number
	 */
	public String getPhone() {
		return this.phone;
	}
	
	/**
	 * Sets this Contact's phone number
	 * 
	 * @param phone this Contact's new phone number
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int compareTo(Contact otherContact) {
		return this.name.compareTo(otherContact.name);
	}
}
