package edu.westga.phonebook.model;

public interface IContactAccessor {
	
	public String getName();
	public String getEmail();
	public String getPhone();
}
