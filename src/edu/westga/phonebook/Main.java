package edu.westga.phonebook;

import java.util.Scanner;

import edu.westga.phonebook.views.PhonebookView;

/**
 * Main class for the Phonebook program
 * 
 * @author lewisb
 *
 */
public class Main {

	/**
	 * Starting point for the program
	 * 
	 * @param args not used
	 */
	public static void main(String[] args) {
		
		PhonebookView view = new PhonebookView();
		Scanner kb = new Scanner(System.in);
		while (true) {
			String command = kb.nextLine();
			boolean status = view.processCommand(command);
			if (!status) {
				kb.close();
				return;
			}
		}
	}

}
